unit DataSetSerializeJSONImpl;

interface

uses superobject, DB, LanguageTypes, ProvidersDataSetSerialize, StrUtils, SysUtils;

type
  TJSONSerialize = class
  private
    FMerging: Boolean;
    FJSONObject: ISuperObject;
    FJSONArray: TSuperArray;
    FOwns: Boolean;
    /// <summary>
    ///   Delete all records from dataset.
    /// </summary>
    /// <param name="ADataSet">
    ///   DataSet that will be cleared.
    /// </param>
    procedure ClearDataSet(const ADataSet: TDataSet);
    /// <summary>
    ///   Load a field of type blob with the value of a JSON.
    /// </summary>
    /// <param name="AField">
    ///   It refers to the field that you want to be loaded with the JSONValue.
    /// </param>
    /// <param name="AJSONValue">
    ///   It refers to the value that is assigned to the field.
    /// </param>
    procedure LoadBlobFieldFromStream(const AField: TField; const AJSONValue:
      ISuperObject);
    /// <summary>
    ///   Loads the fields of a DataSet based on a JSONArray.
    /// </summary>
    /// <param name="AJSONArray">
    ///   JSONArray with the DataSet structure.
    /// </param>
    /// <param name="ADataSet">
    ///   Refers to the DataSet that will be configured.
    /// </param>
    /// <remarks>
    ///   The DataSet can not have predefined fields.
    ///   The DataSet can not be active.
    ///   To convert a structure only JSONArray is allowed.
    /// </remarks>
    procedure JSONArrayToStructure(const AJSONArray: TSuperArray; const ADataSet:
      TDataSet);
    /// <summary>
    ///   Loads a DataSet with a JSONObject.
    /// </summary>
    /// <param name="AJSONObject">
    ///   Refers to the JSON in with the data that must be loaded in the DataSet.
    /// </param>
    /// <param name="ADataSet">
    ///   Refers to the DataSet which must be loaded with the JSON data.
    /// </param>
    /// <param name="AMerging">
    ///   Indicates whether to include or change the DataSet record.
    /// </param>
    procedure JSONObjectToDataSet(const AJSONObject: ISuperObject; const ADataSet:
      TDataSet; const AMerging: Boolean);
    /// <summary>
    ///   Loads a DataSet with a JSONOValue.
    /// </summary>
    /// <param name="AJSONValue">
    ///   Refers to the JSON value that must be loaded in the DataSet.
    /// </param>
    /// <param name="ADataSet">
    ///   Refers to the DataSet which must be loaded with the JSON value.
    /// </param>
    procedure JSONValueToDataSet(const AJSONValue: ISuperObject; const ADataSet:
      TDataSet);
    /// <summary>
    ///   Loads a DataSet with a JSONArray.
    /// </summary>
    /// <param name="AJSONArray">
    ///   Refers to the JSON in with the data that must be loaded in the DataSet.
    /// </param>
    /// <param name="ADataSet">
    ///   Refers to the DataSet which must be loaded with the JSON data.
    /// </param>
    procedure JSONArrayToDataSet(const AJSONArray: TSuperArray; const ADataSet:
      TDataSet);
    /// <summary>
    ///   Creates a JSON informing the required field.
    /// </summary>
    /// <param name="AFieldName">
    ///   Field name in the DataSet.
    /// </param>
    /// <param name="ADisplayLabel">
    ///   Formatted field name.
    /// </param>
    /// <param name="ALang">
    ///   Language used to mount messages.
    /// </param>
    /// <returns>
    ///   Returns a JSON with the message and field name.
    /// </returns>
    function AddFieldNotFound(const AFieldName, ADisplayLabel: string; const ALang:
      TLanguageType = enUS): ISuperObject;
    /// <summary>
    ///   Load field structure.
    /// </summary>
    /// <param name="AJSONValue">
    ///   JSON with field data.
    /// </param>
    /// <returns>
    ///   Record of field structure.
    /// </returns>
    function LoadFieldStructure(const AJSONValue: ISuperObject): TFieldStructure;
    /// <returns>
    ///   The key fields name of the ADataSet parameter.
    /// </returns>
    function GetKeyFieldsDataSet(const ADataSet: TDataSet): string;
    /// <returns>
    ///   The key values of the ADataSet parameter.
    /// </returns>
    function GetKeyValuesDataSet(const ADataSet: TDataSet; const AJSONObject:
      ISuperObject): TKeyValues;
    /// <summary>
    ///   Load the fields into the dataset.
    /// </summary>
    procedure LoadFieldsFromJSON(const ADataSet: TDataSet; const AJSONObject:
      ISuperObject);
  public
    /// <summary>
    ///   Responsible for creating a new instance of TDataSetSerialize class.
    /// </summary>
    /// <param name="AJSONArray">
    ///   Refers to the JSON in with the data that must be loaded in the DataSet.
    /// </param>
    constructor Create(const AJSONArray: TSuperArray; const AOwns: Boolean);
      overload;
    /// <summary>
    ///   Responsible for creating a new instance of TDataSetSerialize class.
    /// </summary>
    /// <param name="AJSONObject">
    ///   Refers to the JSON in with the data that must be loaded in the DataSet.
    /// </param>
    constructor Create(const AJSONObject: ISuperObject; const AOwns: Boolean);
      overload;
    /// <summary>
    ///   Loads fields from a DataSet based on JSON.
    /// </summary>
    /// <param name="ADataSet">
    ///   Refers to the DataSet to which you want to load the structure.
    /// </param>
    procedure LoadStructure(const ADataSet: TDataSet);
    /// <summary>
    ///   Responsible for validating whether JSON has all the necessary information for a particular DataSet.
    /// </summary>
    /// <param name="ADataSet">
    ///   Refers to the DataSet that will be loaded with JSON.
    /// </param>
    /// <param name="ALang">
    ///   Language used to mount messages.
    /// </param>
    /// <returns>
    ///   Returns a JSONArray with the fields that were not informed.
    /// </returns>
    /// <remarks>
    ///   Walk the DataSet fields by checking the required property.
    ///   Uses the DisplayLabel property to mount the message.
    /// </remarks>
    function Validate(const ADataSet: TDataSet; const ALang: TLanguageType = enUS):
      ISuperObject;
    /// <summary>
    ///   Runs the merge between the record of DataSet and JSONObject.
    /// </summary>
    /// <param name="ADataSet">
    ///   Refers to the DataSet that you want to merge with the JSON object.
    /// </param>
    procedure Merge(const ADataSet: TDataSet);
    /// <summary>
    ///   Loads the DataSet with JSON content.
    /// </summary>
    /// <param name="ADataSet">
    ///   Refers to the DataSet you want to load.
    /// </param>
    procedure ToDataSet(const ADataSet: TDataSet);
    /// <summary>
    ///   Responsible for destroying the TJSONSerialize class instance.
    /// </summary>
    /// <remarks>
    ///   If owner of the JSON, destroys the same.
    /// </remarks>
    destructor Destroy; override;
  end;

implementation

uses Classes, TypInfo, DateUtils, ProvidersDataSetSerializeConstants,
  Variants, UpdatedStatusTypes, publicfun4d7;

{ TJSONSerialize }

procedure TJSONSerialize.JSONObjectToDataSet(const AJSONObject: ISuperObject;
  const ADataSet: TDataSet; const AMerging: Boolean);
var
  i: Integer;
  LField: TField;
  LJSONValue: ISuperObject;
  LNestedDataSet: TDataSet;
  //  LBooleanValue: Boolean;
  LDataSetDetails: TList;
  ldate: Tdatetime;
  LObjectState: string;
  Lnote: ISuperObject;
begin
  if (not Assigned(AJSONObject)) or (not Assigned(ADataSet)) then
    Exit;

  if not (ADataSet.Active) then
  begin
    //    if not (ADataSet is TFDMemTable) then
    //      Exit;
    if ADataSet.FieldCount = 0 then
      LoadFieldsFromJSON(ADataSet, AJSONObject);
    ADataSet.Open;
  end;

  if AJSONObject.AsObject.Find(OBJECT_STATE, Lnote) then
  begin
    LObjectState := Lnote.AsString;
    if TUpdateStatusToString(usInserted) = (LObjectState) then
      ADataSet.Append
    else if not (TUpdateStatusToString(usUnmodified) = (LObjectState)) then
    begin
      if not ADataSet.Locate(GetKeyFieldsDataSet(ADataSet),
        VarArrayOf(GetKeyValuesDataSet(ADataSet, AJSONObject)), []) then
        Exit;
      if TUpdateStatusToString(usModified) = (LObjectState) then
        ADataSet.Edit
      else if TUpdateStatusToString(usDeleted) = (LObjectState) then
      begin
        ADataSet.Delete;
        Exit;
      end;
    end;
  end
  else if AMerging then
    ADataSet.Edit
  else
    ADataSet.Append;

  if (ADataSet.State in dsEditModes) then
  begin
    //    for LField in ADataSet.Fields do
    //    begin
    for i := 0 to ADataSet.Fields.Count - 1 do
    begin
      LField := ADataSet.Fields[i];
      if LField.ReadOnly then
        Continue;
      if not (AJSONObject.AsObject.Find(LField.FieldName, LJSONValue) or
        AJSONObject.AsObject.Find(LowerCase(LField.FieldName), LJSONValue)) then
        Continue;
      if LJSONValue = nil then
      begin
        LField.Clear;
        Continue;
      end;
      case LField.DataType of
        ftBoolean:
          //          if LJSONValue.TryGetValue<Boolean > (LBooleanValue) then
          LField.AsBoolean := LJSONValue.AsBoolean;
        ftInteger, ftSmallint: //, ftShortint, ftLongWord:
          LField.AsInteger := StrToIntDef(LJSONValue.AsString, 0);
        ftLargeint, ftAutoInc:
          LField.Value := StrToInt64Def(LJSONValue.AsString, 0);
        ftCurrency:
          LField.AsCurrency := StrToCurr(LJSONValue.AsString);
        ftFloat, ftFMTBcd, ftBCD: //, ftSingle:
          LField.AsFloat := StrToFloat(LJSONValue.AsString);
        ftString, ftWideString, ftMemo: //, ftWideMemo:
          LField.AsString := LJSONValue.AsString;
        ftDate, ftTimeStamp, ftDateTime, ftTime:
          if ISO8601DateToDelphiDateTime(LJSONValue.AsString, ldate) then
            LField.AsDateTime := ldate;
        ftDataSet:
          begin
            LNestedDataSet := TDataSetField(LField).NestedDataSet;

            if LJSONValue.IsType(stArray) then
            begin
              ClearDataSet(LNestedDataSet);
              JSONArrayToDataSet(LJSONValue.AsArray, LNestedDataSet);
            end
            else if LJSONValue.IsType(stObject) then
              JSONObjectToDataSet(LJSONValue, LNestedDataSet, False)
          end;
        ftGraphic, ftBlob: //, ftStream:
          LoadBlobFieldFromStream(LField, LJSONValue);
      else
        raise EDataSetSerializeException.CreateFmt(FIELD_TYPE_NOT_FOUND, [LField.FieldName]);
      end;
    end;
    ADataSet.Post;
  end;
  LDataSetDetails := TList.Create;
  try
    ADataSet.GetDetailDataSets(LDataSetDetails);
    //    for LNestedDataSet in LDataSetDetails do
    //    begin
    for i := 0 to LDataSetDetails.Count - 1 do
    begin
      LNestedDataSet := TDataSet(LDataSetDetails.Items[i]);
      if not
        AJSONObject.AsObject.Find(LowerCase(TDataSetSerializeUtils.FormatDataSetName(LNestedDataSet.Name)), LJSONValue) then
        Continue;
      if LJSONValue = nil then
        Continue;
      if LJSONValue.IsType(stObject) then
        JSONObjectToDataSet(LJSONValue, LNestedDataSet, False)
      else if LJSONValue.IsType(stArray) then
        JSONArrayToDataSet(LJSONValue.AsArray, LNestedDataSet);
    end;
  finally
    LDataSetDetails.Free;
  end;
end;

procedure TJSONSerialize.JSONValueToDataSet(const AJSONValue: ISuperObject;
  const ADataSet: TDataSet);
begin
  if ADataSet.Fields.Count <> 1 then
    raise EDataSetSerializeException.Create(Format(INVALID_FIELD_COUNT, [ADataSet.Name]));
  ADataSet.Append;
  ADataSet.Fields.Fields[0].AsString := AJSONValue.AsString;
  ADataSet.Post;
end;

procedure TJSONSerialize.ToDataSet(const ADataSet: TDataSet);
begin
  if Assigned(FJSONObject) then
    JSONObjectToDataSet(FJSONObject, ADataSet, FMerging)
  else if Assigned(FJSONArray) then
    JSONArrayToDataSet(FJSONArray, ADataSet)
  else
    raise EDataSetSerializeException.Create(JSON_NOT_DIFINED);
end;

function TJSONSerialize.Validate(const ADataSet: TDataSet; const ALang:
  TLanguageType = enUS): ISuperObject;
var
  i: Integer;
  LField: TField;
  LJSONValue: ISuperObject;
begin
  if not Assigned(FJSONObject) then
    raise EDataSetSerializeException.Create(JSON_NOT_DIFINED);
  if ADataSet.Fields.Count = 0 then
    raise EDataSetSerializeException.Create(DATASET_HAS_NO_DEFINED_FIELDS);
  Result := TSuperObject.Create();
  //  for LField in ADataSet.Fields do
  for i := 0 to ADataSet.Fields.Count - 1 do
  begin
    LField := ADataSet.Fields[i];
    if LField.Required then
    begin
      if FJSONObject.AsObject.Find(LField.FieldName, LJSONValue) or
        FJSONObject.AsObject.Find(LowerCase(LField.FieldName), LJSONValue) then
      begin
        if TrimIsEmpty(LJSONValue.AsString) then
          Result.AsArray.Add(AddFieldNotFound(LField.FieldName, LField.DisplayLabel, ALang));
      end
      else if LField.IsNull then
        Result.AsArray.Add(AddFieldNotFound(LField.FieldName, LField.DisplayLabel, ALang));
    end;
  end;
end;

procedure TJSONSerialize.LoadBlobFieldFromStream(const AField: TField; const
  AJSONValue: ISuperObject);
var
  LStringStream: TStringStream;
  LMemoryStream: TMemoryStream;
begin
  LStringStream := TStringStream.Create(AJSONValue.AsString);
  try
    LStringStream.Position := 0;
    LMemoryStream := TMemoryStream.Create;
    try
      Base64Decode(LStringStream.DataString, LMemoryStream);
      TBlobField(AField).LoadFromStream(LMemoryStream);
    finally
      LMemoryStream.Free;
    end;
  finally
    LStringStream.Free;
  end;
end;

procedure TJSONSerialize.LoadFieldsFromJSON(const ADataSet: TDataSet; const
  AJSONObject: ISuperObject);
var
  i: Integer;
  //  JSONPair: TJSONPair;
begin
  for i := 0 to AJSONObject.AsArray.Length - 1 do
  begin
    with ADataSet.FieldDefs.AddFieldDef do
    begin
      Name := AJSONObject.AsArray.S[i];
      DataType := ftString;
      Size := 4096;
    end;
  end;
end;

function TJSONSerialize.LoadFieldStructure(const AJSONValue: ISuperObject):
  TFieldStructure;
var
  LStrTemp: ISuperObject;
  LIntTemp: ISuperObject;
  LBoolTemp: ISuperObject;
begin
  if AJSONValue.AsObject.Find('DataType', LStrTemp) then
    Result.FieldType := TFieldType(GetEnumValue(TypeInfo(TFieldType), LStrTemp.AsString))
  else
    raise EDataSetSerializeException.CreateFmt('Attribute %s not found in json!', ['DataType']);

  if AJSONValue.AsObject.Find('Alignment', LStrTemp) then
    Result.Alignment := TAlignment(GetEnumValue(TypeInfo(TAlignment), LStrTemp.AsString));

  if AJSONValue.AsObject.Find('FieldName', LStrTemp) then
    Result.FieldName := LStrTemp.AsString
  else
    raise EDataSetSerializeException.CreateFmt('Attribute %s not found in json!', ['FieldName']);

  if AJSONValue.AsObject.Find('Size', LIntTemp) then
    Result.Size := LIntTemp.AsInteger;

  if AJSONValue.AsObject.Find('Precision', LIntTemp) then
    Result.Precision := LIntTemp.AsInteger;

  if AJSONValue.AsObject.Find('Origin', LStrTemp) then
    Result.Origin := LStrTemp.AsString;

  if AJSONValue.AsObject.Find('DisplayLabel', LStrTemp) then
    Result.DisplayLabel := LStrTemp.AsString;

  if AJSONValue.AsObject.Find('Key', LBoolTemp) then
    Result.Key := LBoolTemp.AsBoolean;

  if AJSONValue.AsObject.Find('Required', LBoolTemp) then
    Result.Required := LBoolTemp.AsBoolean;

  if AJSONValue.AsObject.Find('Visible', LBoolTemp) then
    Result.Visible := LBoolTemp.AsBoolean;

  if AJSONValue.AsObject.Find('ReadOnly', LBoolTemp) then
    Result.ReadOnly := LBoolTemp.AsBoolean;

  if AJSONValue.AsObject.Find('AutoGenerateValue', LStrTemp) then
    Result.AutoGenerateValue := TAutoRefreshFlag(GetEnumValue(TypeInfo(TAutoRefreshFlag),
      LStrTemp.AsString));
end;

procedure TJSONSerialize.LoadStructure(const ADataSet: TDataSet);
begin
  if Assigned(FJSONObject) then
    raise EDataSetSerializeException.Create(TO_CONVERT_STRUCTURE_ONLY_JSON_ARRAY_ALLOWED)
  else if Assigned(FJSONArray) then
    JSONArrayToStructure(FJSONArray, ADataSet)
  else
    raise EDataSetSerializeException.Create(JSON_NOT_DIFINED);
end;

function TJSONSerialize.AddFieldNotFound(const AFieldName, ADisplayLabel:
  string; const ALang: TLanguageType = enUS): ISuperObject;
begin
  Result := TSuperObject.Create;
  Result.S['field'] := AFieldName;
  case ALang of
    ptBR:
      begin
        Result.S['error'] := ADisplayLabel + ' n�o foi informado(a)';
      end
  else
    Result.S['error'] := ADisplayLabel + ' not informed';
  end;
end;

procedure TJSONSerialize.ClearDataSet(const ADataSet: TDataSet);
begin
  ADataSet.First;
  while not ADataSet.Eof do
    ADataSet.Delete;
end;

constructor TJSONSerialize.Create(const AJSONObject: ISuperObject; const AOwns:
  Boolean);
begin
  FOwns := AOwns;
  FJSONObject := AJSONObject;
end;

constructor TJSONSerialize.Create(const AJSONArray: TSuperArray; const AOwns:
  Boolean);
begin
  FOwns := AOwns;
  FJSONArray := AJSONArray;
end;

procedure TJSONSerialize.JSONArrayToDataSet(const AJSONArray: TSuperArray;
  const ADataSet: TDataSet);
var
  i: Integer;
  LJSONValue: ISuperObject;
begin
  if (not Assigned(AJSONArray)) or (not Assigned(ADataSet)) then
    Exit;
  for i := 0 to AJSONArray.Length - 1 do
  begin
    LJSONValue := AJSONArray.O[i];
    if LJSONValue.IsType(stArray) then
      JSONArrayToDataSet(LJSONValue.AsArray, ADataSet)
    else if LJSONValue.IsType(stObject) then
      JSONObjectToDataSet(LJSONValue, ADataSet, False)
    else
      JSONValueToDataSet(LJSONValue, ADataSet);
  end;
  if ADataSet.Active then
    ADataSet.First;
end;

procedure TJSONSerialize.Merge(const ADataSet: TDataSet);
begin
  FMerging := True;
  try
    ToDataSet(ADataSet);
  finally
    FMerging := False;
  end;
end;

procedure TJSONSerialize.JSONArrayToStructure(const AJSONArray: TSuperArray;
  const ADataSet: TDataSet);
var
  i: Integer;
  //  LJSONValue: ISuperObject;
begin
  if ADataSet.Active then
    raise EDataSetSerializeException.Create(DATASET_ACTIVATED);
  if ADataSet.FieldCount > 0 then
    raise EDataSetSerializeException.Create(PREDEFINED_FIELDS);
  //  for LJSONValue in AJSONArray do
  for i := 0 to AJSONArray.Length - 1 do
    TDataSetSerializeUtils.NewDataSetField(ADataSet, LoadFieldStructure(AJSONArray.O[i]));
end;

destructor TJSONSerialize.Destroy;
begin
  if Assigned(FJSONObject) and FOwns then
    FJSONObject := nil;
  if Assigned(FJSONArray) and FOwns then
    FJSONArray.Free;
  FJSONObject := nil;
  FJSONArray := nil;
  inherited Destroy;
end;

function TJSONSerialize.GetKeyFieldsDataSet(const ADataSet: TDataSet): string;
var
  i: Integer;
  LField: TField;
begin
  Result := EmptyStr;
  //  for LField in ADataSet.Fields do
  for i := 0 to ADataSet.Fields.Count - 1 do
  begin
    LField := ADataSet.Fields[i];
    if pfInKey in LField.ProviderFlags then
      Result := Result + IfThen(TrimIsEmpty(Result), EmptyStr, ';') + LField.FieldName;
  end;
end;

function TJSONSerialize.GetKeyValuesDataSet(const ADataSet: TDataSet; const
  AJSONObject: ISuperObject): TKeyValues;
var
  i: Integer;
  LField: TField;
  LKeyValue: ISuperObject;
begin
  for i := 0 to ADataSet.Fields.Count - 1 do
  begin
    LField := ADataSet.Fields[i];
    if pfInKey in LField.ProviderFlags then
    begin
      if not (AJSONObject.AsObject.Find(LowerCase(LField.FieldName), LKeyValue) or
        AJSONObject.AsObject.Find(LField.FieldName, LKeyValue)) then
        Continue;
      SetLength(Result, Length(Result) + 1);
      Result[Pred(Length(Result))] := LKeyValue.AsString;
    end;
  end;
end;

end.
