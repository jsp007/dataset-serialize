unit publicfun4d7;

interface

uses db, DateUtils, SysUtils, TypInfo,
  Classes, FMTBcd;

function CharInSet(value: char; chars: TSysCharSet): boolean;

function TrimIsEmpty(value: string): Boolean;

function Base64Encode(value: TMemoryStream): string;

function Base64Decode(str: string; Dest: TMemoryStream): string;

implementation

uses IdCoderMIME;

function Base64Decode(str: string; Dest: TMemoryStream): string;
var
  IdDeCoderMIME: TIdDeCoderMIME;
begin
  //  ms := TMemoryStream.Create;
  IdDeCoderMIME := TIdDecoderMIME.Create(nil);
  try
{$IF CompilerVersion <= 15} // d7
    IdDeCoderMIME.DecodeToStream(str, Dest); // indy9
{$ELSE}
    IdDeCoderMIME.DecodeStream(str, Dest); // indy10
{$IFEND}
    Dest.Position := 0;
    //    TZipTools.UnZipStream(ms, stream);  // unzip
  finally
    Dest.Free;
    IdDeCoderMIME.Free;
  end;
end;

function Base64Encode(value: TMemoryStream): string;
var
  IdEncoderMIME: TIdEncoderMIME;
begin
  //ms := TMemoryStream.Create;
  IdEncoderMIME := TIdEncoderMIME.Create(nil);
  try
    //    TZipTools.ZipStream(stream, value); // zip
    value.Position := 0;
{$IF CompilerVersion <= 15} // d7
    Result := IdEncoderMIME.Encode(value);
{$ELSE}
    Result := IdEncoderMIME.EncodeStream(value);
{$IFEND}
  finally
    value.Free;
    IdEncoderMIME.Free;
  end;
end;

function CharInSet(value: char; chars: TSysCharSet): boolean;
begin
  Result := value in chars;
end;

function TrimIsEmpty(value: string): Boolean;
begin
  Result := Trim(value) = '';
end;

end.
