unit BooleanFieldTypes;

interface

type
  /// <summary>
  /// Boolean types handled by the API.
  /// </summary>
  TBooleanFieldType = (bfUnknown, bfBoolean, bfInteger);

function TBooleanFieldTypeTostring(value: TBooleanFieldType): string; overload;
function TBooleanFieldTypeTostring(value: integer): string; overload;
//  TBooleanFieldTypeHelper = class(TBooleanFieldType)
//    function ToString: string;
//  end;

implementation

{ TBooleanFieldTypeHelper }

function TBooleanFieldTypeTostring(value: integer): string; overload;
begin
  result := TBooleanFieldTypeToString(TBooleanFieldType(value));
end;

function TBooleanFieldTypeToString(value: TBooleanFieldType): string;
begin
  case value of
    bfUnknown:
      Result := 'Unknown';
    bfBoolean:
      Result := 'Boolean';
  else
    Result := 'Integer';
  end;
end;

end.
