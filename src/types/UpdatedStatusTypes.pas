unit UpdatedStatusTypes;

interface

uses DB;

function TUpdateStatusToString(value: TUpdateStatus): string;

implementation

{ TUpdateStatusHelper }

function TUpdateStatusToString(value: TUpdateStatus): string;
begin
  case value of
    usModified:
      Result := 'MODIFIED';
    usInserted:
      Result := 'INSERTED';
    usDeleted:
      Result := 'DELETED';
  else
    Result := 'UNOMODIFIED';
  end;
end;

end.
